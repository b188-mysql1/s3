-- [SECTION] Inserting Records/Rows

INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Psy 6",
	"2012-1-1",
	2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Trip",
	"1996-1-1",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Gangnam Style",
	253,
	"K-Pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kundiman",
	234,
	"OPM",
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kisapmata",
	259,
	"OPM",
	2
);

-- [SECTION] Selecting Records

-- Display all columns for all songs
SELECT * FROM songs;

-- Display only the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display only the title of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title and length of OPM songs that are more than 2 minutes and 40 seconds

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- We can use both the AND and OR keywords for multiple expressions in the WHERE clause

-- [SECTION] Updating Records

-- Update the length of Kundiman to 2 minutes and 20 seconds
UPDATE songs SET length = 220 WHERE song_name = "Kundiman";

-- Omitting the WHERE clause will update ALL rows

-- [SECTION] Deleting Records

--Delete all OPM songs that are longer than 2 minutes and 40 seconds
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
